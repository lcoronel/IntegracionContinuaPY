
class Gato:

    def __init__(self, nombre):
        self.nombre = nombre
        self.energia = 50
        self.peso = 2  #kilos

    def trepar(self):
        print("El gato esta trepando alegremente")
        return True

    def correr(self):
        print("El gato esta corriendo animadamente")
        return True

    def comer(self, comida):
        print("Esta comiendo su comidita")
        self.peso = self.peso + comida.peso
        self.energia = self.energia + comida.energia