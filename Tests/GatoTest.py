import unittest

from src.Comida import Comida
from src.Gato import Gato


class GatoTest(unittest.TestCase):


    def setUp(self):
        self.gato = Gato("Spagueti")
        self.comidita = Comida(1,10)

    def test_gato_puede_trepar(self):
        self.assertEqual(True, self.gato.trepar())

    def test_gato_puede_correr(self):
        self.assertEqual(True, self.gato.correr())


    def test_gato_aumenta_peso_al_comer(self):
        peso_inicial = self.gato.peso
        self.gato.comer(self.comidita)
        peso_final = self.gato.peso

        self.assertEqual(peso_final, peso_inicial + self.comidita.peso)


    def test_gato_aumenta_energia_al_comer(self):
        energia_inicial = self.gato.energia
        self.gato.comer(self.comidita)
        energia_final = self.gato.energia

        self.assertEqual(energia_final, energia_inicial + self.comidita.energia)



if __name__ == '__main__':
    unittest.main()
